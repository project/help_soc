<?php

/**
 * @file
 * Admin page callbacks for the help module.
 */

/**
 * Menu callback; prints a page displaying available help topics for modules.
 */
function help_index_page() {
  if (!empty($_GET['popup'])) {
    print help_by_module();
    return;
  }
  else {
    return help_topic_page('help', 'welcome-page', FALSE);
  }
}

/**
 * Menu callback; prints a page displaying available help topics for modules.
 */
function help_by_module() {
  $output = '';
  $breadcrumb = array();
  $menu_items = array();
  $items = array();

  $topics = help_get_topics();
  help_get_topic_hierarchy($topics);

  $modules = module_rebuild_cache();
  foreach ($modules as $file) {
    $module = $file->name;
    if (empty($topics[$module]) || $module == 'help') {
      continue;
    }

    // Fetch help links.
    $items = help_get_tree($topics, $topics[$module]['']['children'], array(), 0);

    // Sort.
    ksort($items);

    $menu_items[$file->info['name']] = array($file->info['description'], $items);
  }

  if (!empty($_GET['popup'])) {
    $GLOBALS['devel_shutdown'] = FALSE; // Prevent devel module from spewing.
    drupal_set_breadcrumb(array_reverse($breadcrumb));
    drupal_add_css(drupal_get_path('module', 'system') . '/admin.css');
    print theme('help_popup', theme('help_by_module', $menu_items));
    return;
  }

  $breadcrumb = array_merge(drupal_get_breadcrumb(), array_reverse($breadcrumb));
  drupal_set_breadcrumb($breadcrumb);

  if (module_exists('search') && user_access('search help')) {
    $output .= drupal_get_form('search_form', &$form_state, '', '', t('Search Help'), NULL);
  }
  $output .= theme('help_by_module', $menu_items);
  return $output;
}

/**
 * Menu callback; prints a page with help topic for a module.
 */
function help_topic_page($module, $topic = NULL, $bells_and_whistles = TRUE) {
  $info = help_get_topic($module, $topic);
  if (isset($topic) && !$info) {
    return drupal_not_found();
  }

  $popup = !empty($_GET['popup']);

  drupal_set_title($info['title']);

  // Set up breadcrumb.
  $breadcrumb = array();

  $parent = $info;
  $pmodule = $module;

  // Loop checker.
  $checked = array();
  while (!empty($parent['parent'])) {
    if (strpos($parent['parent'], '%')) {
      list($pmodule, $ptopic) = explode('%', $parent['parent']);
    }
    else {
      $ptopic = $parent['parent'];
    }

    if (!empty($checked[$pmodule][$ptopic])) {
      break;
    }
    $checked[$pmodule][$ptopic] = TRUE;

    $parent = help_get_topic($pmodule, $ptopic);
    if (!$parent) {
      break;
    }

    $breadcrumb[] = help_l($parent['title'], "admin/help/$pmodule/$ptopic");
  }

  $breadcrumb[] = help_l(help_get_module_name($pmodule), "admin/help/$pmodule");
  $breadcrumb[] = help_l(t('Help'), "admin/help");

  if (!isset($topic)) {
    $topic = 'about';
    drupal_set_title(t('About @module', array('@module' => help_get_module_name($module))));
  }
  $output = help_view_topic($module, $topic, $popup, $bells_and_whistles);
  if (empty($output)) {
    if ($topic == 'about') {
      $output = help_view_module($module, $popup);
    }
    else {
      $output = t('Missing help topic.');
      }
  }

  if ($popup) {
    $GLOBALS['devel_shutdown'] = FALSE; // Prevent devel module from spewing.
    drupal_set_breadcrumb(array_reverse($breadcrumb));
    print theme('help_popup', $output);
    return;
  }

  if ($bells_and_whistles) {
    $breadcrumb[] = help_l('Administer', 'admin');
    $breadcrumb[] = l('Home', '');
    drupal_set_breadcrumb(array_reverse($breadcrumb));
  }
  drupal_add_css(drupal_get_path('module', 'help') . '/help.css');
  return $output;
}


/**
 * Load and render a help topic.
 */
function help_view_module($module, $popup = FALSE) {
  $topics = help_get_topics();
  help_get_topic_hierarchy($topics);
  $items = help_get_tree($topics, $topics[$module]['']['children'], array(), 0);

  $breadcrumb[] = help_l('Help', 'admin/help');

  drupal_set_title(t('@module help index', array('@module' => help_get_module_name($module))));
  return theme('item_list', $items, NULL, 'ul', array('id' => 'toc'));
}

/**
 * Load and render a help topic.
 */
function help_view_topic($module, $topic, $popup = FALSE, $bells_and_whistles = TRUE) {
  $file = help_get_topic_filename($module, $topic);
  $info = help_get_topic($module, $topic);
  if ($file) {
    // @todo is this trusted output?
    $output = file_get_contents($file);
    // Make some exchanges. The strtr is because url() translates $ into %24
    // but we need to change it back for the regex replacement.

    // Run the line break filter if requested
    if (!empty($info['line break'])) {
      // Remove the header since it adds an extra <br /> to the filter.
      $output = preg_replace('/^<!--[^\n]*-->\n/', '', $output);

      $output = _filter_autop($output);
    }

    // Change 'topic:' to the URL for another help topic.
    if ($popup) {
      $output = preg_replace('/href="topic:([^"]+)"/', 'href="' . strtr(url('admin/help/$1', array('query' => 'popup=true')), array('%24' => '$')) . '"', $output);
      $output = preg_replace('/src="topic:([^"]+)"/', 'src="' . strtr(url('admin/help/$1', array('query' => 'popup=true')), array('%24' => '$')) . '"', $output);
    }
    else {
      $output = preg_replace('/href="topic:([^"]+)"/', 'href="' . strtr(url('admin/help/$1'), array('%24' => '$')) . '"', $output);
      $output = preg_replace('/src="topic:([^"]+)"/', 'src="' . strtr(url('admin/help/$1'), array('%24' => '$')) . '"', $output);
    }

    global $base_path;

    // Change 'path:' to the URL to the base help directory.
    $output = preg_replace('/href="path:([^"]+)"/', 'href="' . $base_path . $info['path'] . '/$1"', $output);
    $output = preg_replace('/src="path:([^"]+)"/', 'src="' . $base_path . $info['path'] . '/$1"', $output);

    // Change 'base_url:' to the URL to the site.
    $output = preg_replace('/href="base_url:([^"]+)"/', 'href="' . strtr(url('$1'), array('%24' => '$')) . '"', $output);
    $output = preg_replace('/src="base_url:([^"]+)"/', 'src="' . strtr(url('$1'), array('%24' => '$')) . '"', $output);

    if (!empty($info['navigation']) && !empty($bells_and_whistles)) {
      $topics = help_get_topics();
      help_get_topic_hierarchy($topics);

      if (!empty($topics[$module]['']['children'])) {
        $tree = array($topic);
        if (!empty($topics[$module][$topic]['parent'])) {
          array_push($tree, $topics[$module][$topic]['parent']);
        }
        $items = help_get_tree($topics, $topics[$module]['']['children'], $tree);
        if (count($items) > 1) {
          $output = theme('item_list', $items, NULL, 'ul', array('id' => 'toc-block')) . $output;
        }
      }
      if (!empty($topics[$module][$topic]['children'])) {
        $items = help_get_tree($topics, $topics[$module][$topic]['children']);
        $output .= theme('item_list', $items, NULL, 'ol', array('id' => 'toc'));
      }

      list($parent_module, $parent_topic) = $topics[$module][$topic]['_parent'];
      $siblings = $topics[$parent_module][$parent_topic]['children'];
      uasort($siblings, 'help_uasort');
      $prev = $next = NULL;
      $found = FALSE;
      foreach ($siblings as $sibling) {
        list($sibling_module, $sibling_topic) = $sibling;
        if ($found) {
          $next = $sibling;
          break;
        }
        if ($sibling_module == $module && $sibling_topic == $topic) {
          $found = TRUE;
          continue;
        }
        $prev = $sibling;
      }

      if ($prev || $next) {
        $navigation = '<div class="help-navigation clear-block">';

        $navigation .= '<div class="help-previous">';
        if ($prev) {
          $navigation .= help_l('<< ' . $topics[$prev[0]][$prev[1]]['title'], "admin/help/$prev[0]/$prev[1]");
        }
        $navigation .= '</div>';

        $navigation .= '<div class="help-next">';
        if ($next) {
          $navigation .= help_l($topics[$next[0]][$next[1]]['title'] . ' >>', "admin/help/$next[0]/$next[1]");
        }
        $navigation .= '</div>';

        $navigation .= '</div>';

        $output .= $navigation;
      }
    }

    if (!empty($info['css'])) {
      drupal_add_css($info['path'] . '/' . $info['css']);
    }

    return '<div class="help-topic">' . $output . '</div>';
  }
}

/**
 * Build a tree of help topics.
 */
function help_get_tree($topics, $topic_ids, $tree_parents = array(), $max_depth = -1, $depth = 0) {
  uasort($topic_ids, 'help_uasort');
  $items = array();
  foreach ($topic_ids as $info) {
    list($module, $topic) = $info;
    $item = help_l($topics[$module][$topic]['title'], "admin/help/$module/$topic");
    if (!empty($tree_parents) && $topic == end($tree_parents)) {
      $children = !empty($topics[$module][$topic]['children']) ? $topics[$module][$topic]['children'] : array();
      $item .= theme('item_list', help_get_tree($topics, $children, $tree_parents, $max_depth, $depth + 1));
    }
    else if (empty($tree_parents) && !empty($topics[$module][$topic]['children']) && ($max_depth == -1 || $depth < $max_depth)) {
      $item .= theme('item_list', help_get_tree($topics, $topics[$module][$topic]['children'], $tree_parents, $max_depth, $depth + 1));
    }

    $items[] = $item;
  }

  return $items;
}

/**
 * Build a hierarchy for a single module's topics.
 */
function help_get_topic_hierarchy(&$topics) {
  foreach ($topics as $module => $module_topics) {
    foreach ($module_topics as $topic => $info) {
      $parent_module = $module;
      // We have a blank topic that we don't want parented to
      // itself.
      if (!$topic) {
        continue;
      }

      if (empty($info['parent'])) {
        $parent = '';
      }
      else if (strpos($info['parent'], '%')) {
        list($parent, $parent_module) = explode($info['parent'], '%');
        if (empty($topics[$parent_module][$parent])) {
          // If it doesn't exist, top level.
          $parent = '';
        }
      }
      else {
        $parent = $info['parent'];
        if (empty($module_topics[$parent])) {
          // If it doesn't exist, top level.
          $parent = '';
        }
      }

      if (!isset($topics[$parent_module][$parent]['children'])) {
        $topics[$parent_module][$parent]['children'] = array();
      }
      $topics[$parent_module][$parent]['children'][] = array($module, $topic);
      $topics[$module][$topic]['_parent'] = array($parent_module, $parent);
    }
  }
}

function help_get_help($path = NULL) {
  $output = '';
  $path = isset($path) ? $path : $_GET['q'];
  foreach (module_implements('menu', NULL, TRUE) as $module) {
    $router_items = call_user_func($module . '_menu');
    if (isset($router_items) && is_array($router_items)) {
      foreach (array_keys($router_items) as $item_path) {
        if ($item_path == $path) {
          drupal_alter('menu', $router_items);
          $item = $router_items[$path];
          if (isset($item['help'])) {
            $output .= $item['help'];
            if (isset($item['help topic']) && user_access('access help')) {
              $output .= theme('help_link', $module, $item['help topic'], NULL, TRUE, TRUE, array('class' => 'help-link-align'));
            }
          }
          break;
        }
      }
    }
  }
  return $output;
}

/**
 * Get the information for a single help topic.
 */
function help_get_topic($module, $topic) {
  $topics = help_get_topics();
  if (!empty($topics[$module][$topic])) {
    return $topics[$module][$topic];
  }
}

function _help_check_default_file(&$info, $module, &$path) {
  if (isset($path) && file_exists($path . '/about.html') && empty($info['about'])) {
    $info['about'] = array(
      'title' => t('About @module', array('@module' => help_get_module_name($module))),
      'file' => 'about',
      'weight' => -20,
    );
  }
  else if (empty($info) && empty($path)) {
    $info = array();
    $help_example = drupal_get_path('module', 'help_example') . '/modules';
    $module_path = drupal_get_path('module', $module);
    if (file_exists("$module_path/help/about.html")) {
      $path = "$module_path/help";
      _help_check_default_file($info, $module, $path);
    }
    else if (file_exists("$help_example/$module/about.html")) {
      // Temporary support for core module helps files.
      $path = "$help_example/$module";
      _help_check_default_file($info, $module, $path);
    }
  }
}

/**
 * Search the system for all available help topics.
 */
function help_get_topics() {
  static $topics = NULL;
  if (!isset($topics)) {
    $topics = array();
    $help_example = drupal_get_path('module', 'help_example') . '/modules';

    foreach (module_list() as $module) {
      $module_path = drupal_get_path('module', $module);
      $info = array();
      $path = '';
      if (file_exists("$module_path/help/$module.help")) {
        $path = "$module_path/help";
        $info = parse_ini_file("./$module_path/help/$module.help", TRUE);
        _help_check_default_file($info, $module, $path);
      }
      else if (file_exists("$help_example/$module/$module.help")) {
        // Temporary support for core module helps files in help_example.
        $path = "$help_example/$module";
        $info = parse_ini_file("./$help_example/$module/$module.help", TRUE);
        _help_check_default_file($info, $module, $path);
      }
      else {
        _help_check_default_file($info, $module, $path);
      }

      if (!empty($info)) {
        // Get translated titles:
        global $language;
        if (file_exists("$module_path/translations/help/$language->language/$module.help")) {
          $translation = drupal_parse_info_file("$module_path/translations/help/$language->language/$module.help", TRUE);
        }
        $settings = array();
        if (!empty($info['help settings'])) {
          $settings = $info['help settings'];
          unset($info['help settings']);
        }

        foreach ($info as $name => $topic) {
          // Each topic should have a name, a title, a file and of course the path.
          $file = !empty($topic['file']) ? $topic['file'] : $name;
          $topics[$module][$name] = array(
            'name' => $name,
            'title' => !empty($translation[$name]['title']) ? $translation[$name]['title'] : $topic['title'],
            'weight' => isset($topic['weight']) ? $topic['weight'] : 0,
            'parent' => isset($topic['parent']) ? $topic['parent'] : 0,
            'file' => $file . '.html', // require extension
            'path' => $path, // not in .ini file
            'line break' => isset($topic['line break']) ? $topic['line break'] : (isset($settings['line break']) ? $settings['line break'] : FALSE),
            'navigation' => isset($topic['navigation']) ? $topic['navigation'] : (isset($settings['navigation']) ? $settings['navigation'] : TRUE),
            'css' => isset($topic['css']) ? $topic['css'] : (isset($settings['css']) ? $settings['css'] : NULL),
          );
        }
      }
      $path = '';
      $info = array();
    }
  }
  return $topics;
}

/**
 * Sort + http://php.net/uasort
 */
function help_uasort($id_a, $id_b) {
  $topics = help_get_topics();

  list($module_a, $topic_a) = $id_a;
  $a = $topics[$module_a][$topic_a];

  list($module_b, $topic_b) = $id_b;
  $b = $topics[$module_b][$topic_b];

  $a_weight = isset($a['weight']) ? $a['weight'] : 0;
  $b_weight = isset($b['weight']) ? $b['weight'] : 0;
  if ($a_weight != $b_weight) {
    return ($a_weight < $b_weight) ? -1 : 1;
  }

  if ($a['title'] != $b['title']) {
    return ($a['title'] < $b['title']) ? -1 : 1;
  }
  return 0;
}

/**
 * Load and render a help topic.
 */
function help_get_topic_filename($module, $topic) {
  init_theme();
  global $language;

  $info = help_get_topic($module, $topic);
  if (empty($info)) {
    return;
  }

  // Search paths:
  $paths = array(
    path_to_theme() . '/help', // Allow theme override.
    drupal_get_path('module', $module) . "/translations/help/$language->language", // Translations.
    $info['path'], // In same directory as .inc file.
  );

  foreach ($paths as $path) {
    if (file_exists("./$path/$info[file]")) {
      return "./$path/$info[file]";
    }
  }
}

/**
 * Small helper function to get a module's proper name.
 */
function help_get_module_name($module) {
  $info = db_fetch_object(db_query("SELECT * FROM {system} WHERE name = '%s'", $module));
  $info = unserialize($info->info);
  return t($info['name']);
}

/**
 * Format a link but preserve popup identity.
 */
function help_l($text, $dest, $options = array()) {
  if (!empty($_GET['popup'])) {
    if (empty($options['query'])) {
      $options['query'] = array();
    }

    if (is_array($options['query'])) {
      $options['query'] += array('popup' => TRUE);
    }
    else {
      $options['query'] += '&popup=TRUE';
    }
  }

  return l($text, $dest, $options);
}

/**
 * Format a URL but preserve popup identity.
 */
function help_url($dest, $options = array()) {
  if (!empty($_GET['popup'])) {
    if (empty($options['query'])) {
      $options['query'] = array();
    }

    $options['query'] += array('popup' => TRUE);
  }

  return url($dest, $options);
}

/**
 * Get or create an sid (search id) that correllates to each topic for
 * the search system.
 */
function help_get_sids(&$topics) {
  global $language;
  $result = db_query("SELECT * FROM {help_index} WHERE language = '%s'", $language->language);
  while ($sid = db_fetch_object($result)) {
    if (empty($topics[$sid->module][$sid->topic])) {
      db_query("DELETE FROM {help_index} WHERE sid = %d", $sid->sid);
    }
    else {
      $topics[$sid->module][$sid->topic]['sid'] = $sid->sid;
    }
  }
}

/**
 * Implementation of hook_update_index().
 */
function help_update_index() {
  global $language;

  // If we got interrupted by limit, this will contain the last module
  // and topic we looked at.
  $last = variable_get('help_last_cron', array('time' => 0));
  $limit = intval(variable_get('search_cron_limit', 100));
  $topics = help_get_topics();
  help_get_sids($topics);

  $count = 0;

  foreach ($topics as $module => $module_topics) {
    // Fast forward if necessary.
    if (!empty($last['module']) && $last['module'] != $module) {
      continue;
    }

    foreach ($module_topics as $topic => $info) {
      // Fast forward if necessary.
      if (!empty($last['topic']) && $last['topic'] != $topic) {
        continue;
      }

      // If we've been looking to catch up, and we have, reset so we
      // stop fast forwarding.
      if (!empty($last['module'])) {
        unset($last['topic']);
        unset($last['module']);
      }

      $file = help_get_topic_filename($module, $topic);
      if ($file && (empty($info['sid']) || filemtime($file) > $last['time'])) {
        if (empty($info['sid'])) {
          db_query("INSERT INTO {help_index} (module, topic, language) VALUES ('%s', '%s', '%s')", $module, $topic, $language->language);
          $info['sid'] = db_last_insert_id('help_index', 'sid');
        }

        search_index($info['sid'], 'help', '<h1>' . $info['title'] . '</h1>' . file_get_contents($file));
        $count++;
        if ($count >= $limit) {
          $last['topic'] = $topic;
          $last['module'] = $module;
          // Don't change time if we stop.
          variable_set('help_last_cron', $last);
          return;
        }
      }
    }
  }
  variable_set('help_last_cron', array('time' => time()));
}

function help_preprocess_page(&$vars) {
  $vars['help'] = theme('help_soc');
  $vars['scripts'] = drupal_get_js('header');
  $vars['styles'] = drupal_get_css(drupal_add_css());
  $vars['css'] = drupal_add_css();
}

function theme_help_soc() {
  if ($help = help_get_help()) {
    return '<div class="help">' . $help . '</div>';
  }
}

function theme_help_by_module($menu_items) {
  $stripe = 0;
  $output = '';
  $container = array('left' => '', 'right' => '');
  $flip = array('left' => 'right', 'right' => 'left');
  $position = 'left';

  // Iterate over all modules
  foreach ($menu_items as $module => $block) {
    list($description, $items) = $block;

    // Output links
    if (count($items)) {
      $block = array();
      $block['title'] = $module;
      $block['content'] = theme('item_list', $items);
      $block['description'] = t($description);

      if ($block_output = theme('admin_block', $block)) {
        if (!isset($block['position'])) {
          // Perform automatic striping.
          $block['position'] = $position;
          $position = $flip[$position];
        }
        $container[$block['position']] .= $block_output;
      }
    }
  }

  $output = '<div class="admin clear-block">';
  foreach ($container as $id => $data) {
    $output .= '<div class="' . $id . ' clear-block">';
    $output .= $data;
    $output .= '</div>';
  }
  $output .= '</div>';

  return $output;
}

/**
 * Display a help icon with a link to view the topic in a popup.
 *
 * @param $module
 *   The module that owns this help topic.
 * @param $topic
 *   The identifier for the topic
 * @param $type
 *   - 'icon' to display the question mark icon
 *   - 'title' to display the topic's title
 *   - any other text to display the text. Be sure to t() it!
 */
function theme_help_link($module, $topic = NULL, $label = NULL, $icon = TRUE, $popup = TRUE, $attributes = array()) {
  drupal_function_exists('help_get_topic');
  $info = help_get_topic($module, $topic);
  if (isset($topic) && !$info) {
    return;
  }

  $output = '<div class="help-link-inline' . ($icon ? ' help-link-icon' : '') . (isset($attributes['class']) ? ' ' . $attributes['class'] : '') . '">';

  $output .= t('<a class="@class" href="@link" title="!title">!label</a>', array(
    '@class' => $popup ? 'help-link-popup' : '',
    '@link' => url(trim("admin/help/$module/$topic", '/')),
    '!title' => $info['title'],
    '!label' => '<span>' . (isset($label) ? t($label) : t('More help')) . '</span>',
  ));

  $output .= '</div>';

  $module_path = drupal_get_path('module', 'help');
  drupal_add_css($module_path . '/help-link.css');
  drupal_add_js($module_path . '/help.js');

  return $output;
}
// Not sure why these aren't getting added when above theme function is
// fired for short help viz usually shown above pages. Hence, temp. fix:
$module_path = drupal_get_path('module', 'help');
drupal_add_css($module_path . '/help-link.css');
drupal_add_js($module_path . '/help.js');

/**
 * Fill in a bunch of page variables for our specialized popup page.
 */
function template_preprocess_help_popup(&$variables) {
  // Add favicon.
  if (theme_get_setting('toggle_favicon')) {
    drupal_set_html_head('<link rel="shortcut icon" href="'. check_url(theme_get_setting('favicon')) .'" type="image/x-icon" />');
  }

  global $theme;
  // Construct page title.
  if (drupal_get_title()) {
    $head_title = array(strip_tags(drupal_get_title()), variable_get('site_name', 'Drupal'));
  }
  else {
    $head_title = array(variable_get('site_name', 'Drupal'));
    if (variable_get('site_slogan', '')) {
      $head_title[] = variable_get('site_slogan', '');
    }
  }

  $module_path = drupal_get_path('module', 'help');
  drupal_add_css($module_path . '/help-popup.css');
  drupal_add_css($module_path . '/help.css');

  $variables['head_title']        = implode(' | ', $head_title);
  $variables['base_path']         = base_path();
  $variables['front_page']        = url();
  $variables['breadcrumb']        = theme('breadcrumb', drupal_get_breadcrumb());
  $variables['feed_icons']        = drupal_get_feeds();
  $variables['head']              = drupal_get_html_head();
  $variables['language']          = $GLOBALS['language'];
  $variables['language']->dir     = $GLOBALS['language']->direction ? 'rtl' : 'ltr';
  $variables['logo']              = theme_get_setting('logo');
  $variables['messages']          = theme('status_messages');
  $variables['site_name']         = (theme_get_setting('toggle_name') ? variable_get('site_name', 'Drupal') : '');
  $variables['css']               = drupal_add_css();
  $css = drupal_add_css();

  // Remove theme css.
  foreach ($css as $media => $types) {
    if (isset($css[$media]['theme'])) {
      $css[$media]['theme'] = array();
    }
  }

  $variables['styles']            = drupal_get_css($css);
  $variables['scripts']           = drupal_get_js();
  $variables['title']             = drupal_get_title();
  // Closure should be filled last.
  $variables['closure']           = theme('closure');
}



Drupal.behaviors.help = function() {
  $('.help-link-popup').each(function() {
    $(this).bind('click', function() {
      var url = this.href + (this.href.indexOf('?') != -1 ? '&' : '?') + "popup=1";
      window.open(url, 'help_window', 'width=600,height=550,scrollbars,resizable'); return false;
    });
  });
}
